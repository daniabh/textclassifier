package gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import classifier.interfaces.ICategorizer;
import classifier.interfaces.IDataSetSerializer;
import classifiers.shared.LabelledTextList;
import classifiers.shared.MultiCategorizer;
import classifiers.shared.SingleCategorizer;
import fileio.languages.WiliLanguageSerializer;
import fileio.tweets.TweetSerializer;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * LoadTab allows the user to choose the type of dataset they want to load (Genre,Emotion,Language)
 * @author Dania Bouhmidi
 */
public class LoadTab extends Tab {

	private TextField loadingPath;
	private String selected;
	private Button loadButton;

	private  ObservableList<CategorizerInfo> categorizerInformation = FXCollections.observableArrayList();

	/**
	 * Constructor : creates a LoadTab by called its base class's constructor
	 * Sets the categorizerInformation list
	 * Sets the content of the Tab with the createUserInterface() method
	 * @param categorizerInformation
	 */
	public LoadTab( ObservableList<CategorizerInfo> categorizerInformation ) {
		super("Load");
		this.categorizerInformation = categorizerInformation;
		this.setContent(createUserInterface());

	}

	/**
	 * Returns selected category (Genre,Emotion,Language)
	 * @return String representing the selected category 
	 */
	public String getSelected() {
		return selected;
	}
	/**
	 * Returns the loading path given as input by the user
	 * @return loadingPath.getText() (loading path)
	 */
	public String getLoadingPath() {
		return this.loadingPath.getText();
	}


	/**
	 * Creates user interface : Vbox inside a StackPane
	 * Contains a Label : "Type the root folder here"
	 * Contains a TextField for the user path input
	 * Contains three radio buttons for the dataset choice
	 * Contains a Load Button that is enabled based on validateEnteredData() 
	 * @return a StackPane containing the vbox
	 */

	private StackPane createUserInterface() {
		StackPane stackpane = new StackPane();
		stackpane.setAlignment(Pos.CENTER);

		stackpane.setPadding(new Insets(25, 25, 25, 25));

		VBox content = new VBox(); 

		ToggleGroup group = new ToggleGroup();

		RadioButton loadLanguage = new RadioButton("Language");
		loadLanguage.setUserData("Language");
		loadLanguage.setToggleGroup(group);

		RadioButton loadGenre = new RadioButton("Genre");
		loadGenre.setUserData("Genre");
		loadGenre.setToggleGroup(group);

		RadioButton loadEmotion = new RadioButton("Emotion");
		loadEmotion.setUserData("Emotion");
		loadEmotion.setToggleGroup(group);
		Label loadPath = new Label("Type the root folder here");
		loadingPath = new TextField ();
		content.getChildren().add(loadPath);
		content.getChildren().add(loadingPath);

		content.getChildren().addAll(loadLanguage,loadGenre,loadEmotion);

		loadButton = new Button ("Load");

		content.getChildren().add(loadButton);
		loadButton.setDisable(true);

		group.selectedToggleProperty().addListener(new ChangeListener<Toggle>(){
			public void changed(ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) {

				if (group.getSelectedToggle() != null) {
					selected = group.getSelectedToggle().getUserData().toString();
					if(validateEnteredData()) {
						loadButton.setDisable(false);
					}
				}

			} 
		});

		stackpane.getChildren().add(content);
		return stackpane;
	}



	/**
	 * Sets the EventHandler of the Load Button 
	 * @param e (EventHandler object)
	 */
	public void setLoadAction(EventHandler<ActionEvent> e) {
		this.loadButton.setOnAction(e);
	}



	/**
	 * Validates that user entered info 
	 * @return boolean representing whether the user has entered the necessary info
	 */
	private boolean validateEnteredData() {

		if (loadingPath.getText().isEmpty() || selected == null) {
			return false;
		}
		return true;
	}


}
