package gui;

import java.io.IOException;
import java.util.Arrays;

import classifier.interfaces.ICategorizer;
import classifier.interfaces.IDataSetSerializer;
import classifiers.shared.LabelledTextList;
import classifiers.shared.MultiCategorizer;
import classifiers.shared.SingleCategorizer;
import fileio.genres.BbcGenreSerializer;
import fileio.languages.WiliLanguageSerializer;
import fileio.tweets.TweetSerializer;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TabPane;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


public class MainApplication extends Application{
  /**
   * MainApplication creates a List<CategorizerInfo> which will be passed to the other classes (via the
   * constructors for those classes) and will store the categorizers that have been loaded
   *  Creates a TabPane with 3 tabs, LoadTab,LabellingTab and CategorizeTab
   *  @author Dania Bouhmidi & Michelle Polonsky
   */
  private static ObservableList<CategorizerInfo> categorizerInformation = FXCollections.observableArrayList();
  
  public static void main(String[] args) {
    Application.launch(args);
    
  }
  
  @Override
  public void start(Stage primaryStage) {
    
    Group root = new Group();
    Scene scene = new Scene(root, 500,500, Color.WHITE);
    root.getChildren().add(createTabs());
    primaryStage.setScene(scene);
    primaryStage.setTitle("Text Categorizers");
    primaryStage.show();
  }
  
  
  /**
   * Creates three tabs, a loadTab, a LabellingTab and a CategorizeTab
   * and return them inside a tabPane
   * @return TabPane containing the 3 tabs
   */
  public TabPane createTabs () {
    TabPane tabPane = new TabPane();
    LoadTab loadTab = new LoadTab(categorizerInformation);
    LabellingTab labellingTab = new LabellingTab (categorizerInformation);
    CategorizingTab categorizingTab = new CategorizingTab (categorizerInformation);
    
    loadTab.setLoadAction( (new EventHandler<ActionEvent>() {
      @Override 
      public void handle(ActionEvent e) {
        String categorizerType = null;
        IDataSetSerializer serializer = null;
        LabelledTextList labelledList = null;
        ICategorizer categorizer = null;
        try {
          switch (loadTab.getSelected()) {
            
            case "Language":
              categorizerType = "Language";
              serializer = new WiliLanguageSerializer(loadTab.getLoadingPath());
              labelledList = new LabelledTextList(serializer.load().getProblemSet());
              labelledList.addAll(serializer.load());
              categorizer = new SingleCategorizer(labelledList,true);
              labellingTab.setCategory("Language");
              break;
              
            case "Genre":              
              categorizerType = "Genre";
              serializer = new BbcGenreSerializer(loadTab.getLoadingPath());
              labelledList = new LabelledTextList(serializer.load().getProblemSet());
              labelledList.addAll(serializer.load());
              categorizer = new SingleCategorizer(labelledList,true);
              labellingTab.setCategory("Genre");        
              break;
              
            case "Emotion":
              categorizerType = "Emotion";
              serializer = new TweetSerializer(loadTab.getLoadingPath());
              labelledList = new LabelledTextList(serializer.load().getProblemSet());
              labelledList.addAll(serializer.load());
              categorizer = new MultiCategorizer(labelledList,true);;
              labellingTab.setCategory("Emotion");
              break;
              
          }
          Arrays.sort(labelledList.getProblemSet().getCategoryNames()); // sort labels alphabetically          
          
          //clear combo box
          labellingTab.getChoice().getItems().clear();
          //add labels to combo box
          labellingTab.getChoice().getItems().addAll(labelledList.getProblemSet().getCategoryNames());
          CategorizerInfo info = new CategorizerInfo(labelledList,categorizer);
          info.setCategorizer(categorizerType);
          categorizerInformation.add(info);          
        }
        catch(IOException o){          
          Alert alert = new Alert(AlertType.INFORMATION);
          alert.setTitle("IllegalFilePath");
          alert.setContentText(loadTab.getLoadingPath() + " is an invalid file path");
          alert.showAndWait(); 
        }       
        
      }
      
    }
    ));    
    
    tabPane.getTabs().addAll(loadTab,labellingTab,categorizingTab);
    return tabPane;
  }   
}
