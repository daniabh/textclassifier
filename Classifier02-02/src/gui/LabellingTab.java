package gui;

import java.io.IOException;
import java.util.List;

import classifier.interfaces.IDataSetSerializer;
import classifiers.shared.LabelledText;
import fileio.genres.BbcGenreSerializer;
import fileio.languages.WiliLanguageSerializer;
import fileio.tweets.TweetSerializer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 *  LabellingTab is a Tab that allows the user to write text into a TextArea,
 *  Choose its category (Language,Genre,Emotion)
 * 	Add it as a new LabelledText to the labelledlist of the loaded serializer
 *  And save the labelledlist into a folder (given by the user)
 *  @author Dania Bouhmidi
 */
public class LabellingTab extends Tab {

	private  ObservableList<CategorizerInfo> categorizerInformation = FXCollections.observableArrayList();
	private ComboBox <String>labelChoice = new ComboBox<String>();
	private String selectedCategory;

	/**
	 * Constructor : takes as input an ObservableList <CategorizerInfo>
	 * Creates a LabellingTab and sets its content
	 * @param categorizerInformation
	 */
	public LabellingTab(ObservableList<CategorizerInfo> categorizerInformation) {
		super("Labelling");
		this.categorizerInformation = categorizerInformation;
		this.setContent(createUserInterface());

	}
	/**
	 * Allows us to retrieve the 
	 * selected category (Emotion,Genre,Language) through a String variable
	 * @param category
	 */
	public void setCategory (String category) {
		this.selectedCategory = category;
	}
	/**
	 * Returns the combo box of label choices
	 * @return ComboBox containing label choices
	 */
	public ComboBox getChoice () {
		return this.labelChoice;
	}


	/**
	 * Sets the labellingTab content :
	 * A TextArea where the user can write text (that will be added to the labelledTextList of the loaded serializer)
	 * Radio Buttons to choose a category (Genre,Emotion,Language)
	 * A ComboBox containing the label choice for the chosen category
	 * An addLabel  Button (when it is clicked, adds that text and the selected label to the loaded list)
	 * An Export Button (when it is clicked, it saves the labelledlist to a folder given by the user
	 * A TextField (where the user can give a path where to save the labelledlist to)
	 * @return a StackPane containing all these elements
	 */
	private StackPane createUserInterface() {
		StackPane stackpane = new StackPane();  
		stackpane.setAlignment(Pos.CENTER);

		stackpane.setPadding(new Insets(25, 25, 25, 25));

		VBox content = new VBox( );

		ToggleGroup group = new ToggleGroup();

		RadioButton loadLanguage = new RadioButton("Language"); 
		loadLanguage.setUserData("Language");
		loadLanguage.setToggleGroup(group);

		RadioButton loadGenre = new RadioButton("Genre");
		loadGenre.setUserData("Genre");
		loadGenre.setToggleGroup(group);

		RadioButton loadEmotion = new RadioButton("Emotion");
		loadEmotion.setUserData("Emotion");
		loadEmotion.setToggleGroup(group);
		TextArea text = new TextArea();
		content.getChildren().add(text);
		content.getChildren().addAll(loadLanguage,loadGenre,loadEmotion);


		content.getChildren().add(labelChoice);

		Button addLabel = new Button("Add label");
		content.getChildren().add(addLabel);
		addLabel.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				String selected = group.getSelectedToggle().getUserData().toString();
				for (CategorizerInfo info : categorizerInformation) {

					if (selected.equals(info.toString())) {
						info.getLabelledList().add(new LabelledText(labelChoice.getValue().toString(),text.getText()));
					}
				}
			}
		}
				);

		TextField exportPath = new TextField();
		content.getChildren().add(exportPath);
		Button exportButton = new Button("Export");
		content.getChildren().add(exportButton);
		exportButton.setOnAction(new EventHandler <ActionEvent>(){

			@Override
			public void handle(ActionEvent arg0) {

				//create serializer and save
				try {
					IDataSetSerializer serializer = null;
					switch (selectedCategory) {
					case "Language":
						serializer = new WiliLanguageSerializer(exportPath.getText());
						for (CategorizerInfo info : categorizerInformation) {
							if ("Language".equals(info.toString())) {
								serializer.save(info.getLabelledList());
							}
						}
						break;
					case "Genre":
						serializer = new BbcGenreSerializer(exportPath.getText());
						for (CategorizerInfo info : categorizerInformation) {
							if ("Genre".equals(info.toString())) {
								serializer.save(info.getLabelledList());
							}
						}
						break;
					case "Emotion":
						serializer = new TweetSerializer(exportPath.getText());
						for (CategorizerInfo info : categorizerInformation) {
							if ("Emotion".equals(info.toString())) {
								serializer.save(info.getLabelledList());
							}
						}
						break;
					}
				}
				catch (IOException e) {
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("IllegalFilePath");
					alert.setContentText(exportPath.getText() + " is an invalid file path");
					alert.showAndWait();  
				}
			}

		}
				);
		stackpane.getChildren().add(content);


		return stackpane;

	}



}
