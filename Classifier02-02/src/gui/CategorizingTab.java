package gui;

import java.io.IOException;
import java.util.List;
import fileio.genres.BbcGenreSerializer;
import fileio.languages.WiliLanguageSerializer;
import fileio.tweets.TweetSerializer;
import javafx.collections.*;
import javafx.event.*;
import javafx.geometry.*;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import classifier.interfaces.*;

/**
 * CategorizingTab is used to created a new tab in which a text can be entered and its language,
 * genre, and emotion is categorized.
 * @author Michelle Polonsky
 *
 */
public class CategorizingTab extends Tab{
	
	private ObservableList<CategorizerInfo> categorizerInfo = FXCollections.observableArrayList();
	private TextArea answer;
	private TextField text;
	private Button categorize;
		
	public CategorizingTab(ObservableList<CategorizerInfo> categorizerInfo) {
		super("Categorizing");
		this.categorizerInfo = categorizerInfo;
		this.setContent(createUserInterface());
	}
	
	private StackPane createUserInterface() {
		StackPane stackpane = new StackPane();  
		stackpane.setAlignment(Pos.CENTER);
		stackpane.setPadding(new Insets(25, 25, 25, 25));
		VBox content = new VBox();
		
		text = new TextField();
		categorize = new Button("Categorize!");
		answer = new TextArea();
		content.getChildren().add(text);
		content.getChildren().add(categorize);

		categorize.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				String language = "";
				String genre = "";
				String emotion = "";
				for(CategorizerInfo info : categorizerInfo) {
					if(info.toString().equals("Genre")){//if Genre Categorizer
						genre = info.getCategorizer().categorize(text.getText());//categorize the entered text
					}
					if(info.toString().equals("Emotion")){//if Emotion Categorizer
						emotion = info.getCategorizer().categorize(text.getText());
					}
					if(info.toString().equals("Language")){//if Language categorizer
						language = info.getCategorizer().categorize(text.getText());
					}
				}//for 
				answer.setText("This program has determined: \n"+language+"\n"+genre+"\n"+emotion);
				content.getChildren().add(answer);
			}//handle
		});//EventHandler // setOnAction
		
		stackpane.getChildren().add(content);
		return stackpane;
	}//createUserInterface
	
}
