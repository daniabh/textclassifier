package classifiers.shared;
import classifier.interfaces.IProblemSet;
import classifier.utilities.ClassifierUtilities;

/**
 * @author Dania Bouhmidi
 * SingleProblemSet is a class that implements the IProblemSet interface
 * 	SingleProblemSet stores a boolean to determine if the categories inside its categoryNames [] field are unique
 *  SingleProblemSet determines whether a given category is contained inside its  inside its categoryNames [] field 
 */
public class SingleProblemSet implements IProblemSet {
	private  String [] categoryNames;
	private boolean unique;
	public String [] getCategoryNames(){
		return this.categoryNames;
	}
	/**
	 * Constructor : takes as input a String array containing category names, and a boolean value representing its uniqueness.
	 * @param categoryNames : String [] containing multiple categories
	 * @param unique : boolean representing whether categoryNames contains unique categories
	 */
	public SingleProblemSet (String [] categoryNames, boolean unique) {
		this.categoryNames = new String [categoryNames.length];
		ClassifierUtilities.copyArray(this.categoryNames, categoryNames);
		this.unique = unique;
	}

	/**
	 * Returns a boolean value indicating whether the SingleProblemSet object's categoryNames is unique
	 * @return unique : boolean representing whether categoryNames contains unique categories
	 */
	public boolean isUnique() {
		return unique;
	}

	/**
	 * Determines whether the input category is contained inside SingleProblemSet's categoryNames[]
	 * @return boolean indicating whether the input category is contained inside the categoryNames array
	 */
	public boolean containsCategory(String category) {

		for (int i = 0; i < this.categoryNames.length; i++) {
			if (category.equals(this.categoryNames[i])){
				return true;
			}
		}
		return false;
	}
}

