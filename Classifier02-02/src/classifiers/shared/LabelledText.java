package classifiers.shared;

/**
 * LabelledText is an immutable class that implements the Comparable interface.
 * It has two fields, a String category and a String text with a getter for each
 * of them along with a constructor to set the two fields. It has a compareTo
 * method. The purpose of this class is to pair a text with its category.
 * 
 * @version 2019-10-20
 * @author Dania Bouhmidi
 */
public final class LabelledText implements Comparable<LabelledText> {
	private final String category;
	private final String text;

	/**
	 * Returns a String representing a LabelledText's category
	 * 
	 * @return String representing a LabelledText's category
	 */
	public String getCategory() {
		return this.category;
	}

	/**
	 * Returns a String representing a LabelledText's text
	 * 
	 * @return String representing a LabelledText's text
	 */
	public String getText() {
		return this.text;
	}

	/**
	 * Constructor : Creates a LabelledText object by setting its category and text
	 * 
	 * @param category (A language or a topic)
	 * @param text     (A text document)
	 */
	public LabelledText(String category, String text) {
		this.category = category;
		this.text = text;
	}

	/**
	 * @Override
	 * Compares two LabelledTexts'texts lexicographically. If this LabelledText's
	 * text is equal to the other LabelledText's text, both of their categories will
	 * be compared lexicographically too.
	 * 
	 * @return int representing whether this LabelledText's text is
	 *         lexicographically greater than the other LabelledText's text. If it
	 *         is lexicographically greater, (1) If it is lexicographilly smaller,
	 *         (-1) If it is lexicographically equal, the category is compared: If
	 *         the text's category is lexicographically greater, (1) If the text's
	 *         category is lexicographilly smaller, (-1) If the categories are
	 *         lexicographically equal (0)
	 */
	public final int compareTo(LabelledText other) {

		if (this.text.compareTo(other.text) > 0) {
			return 1;
		}
		if (this.text.compareTo(other.text) < 0) {
			return -1;
		}

		else {
			if (this.category.compareTo(other.category) > 0) {
				return 1;
			}
			if (this.category.compareTo(other.category) < 0) {
				return -1;
			}

			return 0;
		}
	}
}
