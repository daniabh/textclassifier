package classifiers.shared;
import java.util.Arrays;

import classifier.utilities.ClassifierUtilities;

public class MultiCategorizer extends CategorizerBase{

	private LabelledTextList list;
	private String sortedCategories[][];
	private boolean removePunctuation;


	/**
	 * @author Dania Bouhmidi
	 * MultiCategorizer serves as a categorizer for a LabelledTextList list that contains multiple categories
	 * MultiCategorizer enables us to determine the most frequent categories by counting the amount of words of each category
	 * MultiCategorizer categorizes the String elements contained inside the LabelledTextList list to one or (multiple)specific categories, (most common categories)
	 */



	/**
	 * Constructor : initializes a MultiCategorizer object with a LabelledText list and a boolean
	 * representing whether the punctuation should be removed from the LabeleldTexts inside the LabelledTextList
	 * It takes the input and sorts it by category, removes the duplicates and punctuation
	 * @param A LabelledText list
	 * @param A boolean representing whether the punctuation should be removed from the LabeleldTexts inside the LabelledTextList
	 */
	public MultiCategorizer (LabelledTextList list,boolean removePunctuation ) {
		this.list = new LabelledTextList (list.getProblemSet());
		this.list.addAll(list);
		this.removePunctuation = removePunctuation;
		sortByCategory(list);
	}




	/* Returns an array of alphabetically sorted words of a specific category
	 * @return An array of alphabetically sorted words of a specific category
	 */ 
	public String [] getSentence (int index) {
		return this.sortedCategories[index];
	}


	/**
	 *Takes as input an index and returns the category at that index
	 *inside the LabelledTextList
	 *@param index of the category
	 *@return A String indicating the category at that index
	 */ 
	public String getCategory(int index) {
		return this.list.get(index).getCategory();
	}

	/**
	 *Takes as input a LabelledTextList list, sorts it
	 *Searches through that array for indexes of each label,
	 *Initializes array inside sortedCategories with specific size for each category
	 *Copies the data at those indexes into their category's specific array 
	 *If a LabelledText's text contains multiples categories, the text will be stored in each category's specific array
	 *Removes duplicates, punctuation(based on the removePunctuation boolean)  and upper case letters from those arrays
	 * @param LabelledText list  to be sorted 
	 * @return void
	 */ 
	private void sortByCategory(LabelledTextList labelledList){

		// initalizes the sorted arrays with corresponding sizes
		sortedCategories = new String [this.list.getProblemSet().getCategoryNames().length][];



		//If a labelledText contains multiple categories (if its getCategory string contains the pattern ";"), 
		//store words into all of the categories delimited by that pattern


		for (LabelledText element : labelledList) {

			if (element.getCategory().contains(";")) {
				for (int j =0; j<this.list.getProblemSet().getCategoryNames().length; j++) {
					for (int s =0; s<labelledList.size(); s++) {
						String [] multiplecats = labelledList.get(s).getCategory().split(";");

						for (int m =0; m<multiplecats.length; m++) {
							if (multiplecats[m].equals((this.list.getProblemSet().getCategoryNames()[j])) ) {
								sortedCategories[j] =	 ClassifierUtilities.addArrays(sortedCategories[j], labelledList.get(s).getText().split(" ") );

							}
						}
					}
				}


			}

			else {

				for (int i =0; i<this.list.getProblemSet().getCategoryNames().length; i++){
					if (element.getCategory().equals(this.list.getProblemSet().getCategoryNames()[i])) {                   
						sortedCategories[i]=  ClassifierUtilities.addArrays(sortedCategories[i], element.getText().split(" ") );
					}

				}
			}
		}







		if (removePunctuation) {
			LabelledTextList listWithoutPunctuation = new LabelledTextList(list.getProblemSet());
			LabelledText [] nopunctuation =  new LabelledText[list.size()];

			for (int i1 =0; i1< list.size(); i1++ ) {  
				nopunctuation[i1] = new LabelledText(list.get(i1).getCategory(), list.get(i1).getText().replaceAll("\\p{Punct}",""));
			}

			listWithoutPunctuation.addAll( Arrays.asList(nopunctuation));
			list = listWithoutPunctuation;
		}


		for (int i1 =0; i1<sortedCategories.length; i1++){
			sortedCategories[i1] = ClassifierUtilities.getArrayasString(sortedCategories[i1]).toLowerCase().split(" ");
			sortedCategories[i1] = ClassifierUtilities.removeDuplicates (ClassifierUtilities.getArrayasString(sortedCategories[i1]).split(" "));
			ClassifierUtilities.alphabeticalSort(sortedCategories[i1]);

		}



	}


	@Override
	/**
	 *Takes as input a String which represents an entire document/text
	 *Determines its category based on the frequency of each category contained inside list.getProblemSet().getCategoryNames()
	 *Keeps track of a category's occurence with an int[] (isCategory[])
	 *Returns the category which occurs the most frequently, 
	 *Can return multiple categories delimited by the ";" character
	 *@param A string representing an entire document/text
	 *@return A string representing the category that occurs the most frequently
	 */ 
	public String categorize(String text) {

		String [] document = text.split(" ");
		// only remove punctuation if boolean is true
		if (removePunctuation) {
			document = ClassifierUtilities.removePunctuationandCase(document);
		}
		//only remove uppercase, not punctuation
		else {
			for (int i =0; i<document.length; i++) {
				document[i].toLowerCase();
			}
		}

		document = ClassifierUtilities.removeDuplicates(document);
		ClassifierUtilities.alphabeticalSort(document);
		int [] isCategory = new int [list. getProblemSet().getCategoryNames().length];
		for (int i =0; i<sortedCategories.length; i++) {
			for (String word : sortedCategories[i]){
				if (ClassifierUtilities.find(word,document) == 0){
					//tracks category occurences
					isCategory[i]++;
					
				}

			}
		}
		System.out.println(Arrays.toString(isCategory));
		StringBuilder multipleCategories = new StringBuilder("");
		int category_count =0; 
		
		for (int i =0; i<isCategory.length; i++) {
			//System.out.println( list. getProblemSet() .getCategoryNames()[i] );
			if (isCategory[i] > (document.length/2))
			{
				
				category_count++;
				multipleCategories.append(list. getProblemSet() .getCategoryNames()[i] +";" );
				
			}

		}
	
		//if text contained one or multiple categories
		if (category_count >=1 ) {
			//remove last occurence of ";" character since we do not need it
			multipleCategories.deleteCharAt(multipleCategories.lastIndexOf(";"));
		
			return multipleCategories.toString();
		}
		

		return null;

	}


}
