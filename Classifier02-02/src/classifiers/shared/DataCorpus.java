package classifiers.shared;

import classifier.interfaces.IProblemSet;
import classifier.utilities.ClassifierUtilities;

/**
 * @author Dania Bouhmidi and Michelle Polonsky
 * A DataCorpus object has three fields: LabelledText[] trainingSet,
 * LabelledText[] testSet, and IProblemSet categories.
 */
public class DataCorpus {
	private LabelledText[] trainingSet;
	private LabelledText[] testSet;
	private IProblemSet categories;

	/**
	 * Constructor : takes as input a training LabelledText[], a testing
	 * LabelledText[] and an object implementing IProblemSet.
	 * It sets the fields and validates that every LabelledText has a category that is part of the valid
	 * categories returned by the categories.getCategoryNames() method.
	 * @param LabelledText[] trainingSet (array of labelled texts used for training)
	 * @param LabelledText[] testSet (array of labelled texts used for testing)
	 * @param IProblemSet    categories (object implementing the IProblemSet
	 *                       interface)
	 */
	public DataCorpus(LabelledText[] trainingSet, LabelledText[] testSet, IProblemSet categories) {
		
		
			int train_categorycount = 0;
			int test_categorycount = 0;
			// checks that each LabelledText has has a category that is part of the valid
			// categories returned by the categories object
			for (int i = 0; i < trainingSet.length; i++) {
				for (int j = 0; j < categories.getCategoryNames().length; j++) {
					if (trainingSet[i].getCategory().equals(categories.getCategoryNames()[j])) {
						train_categorycount++;
					}

				}
			}

			for (int i = 0; i < testSet.length; i++) {
				for (int j = 0; j < categories.getCategoryNames().length; j++) {

					if (testSet[i].getCategory().equals(categories.getCategoryNames()[j])) {
						test_categorycount++;
					}

				}
			}
               //fields are initialized and set only if every LabelledText has a valid category
			if (test_categorycount == testSet.length && train_categorycount == trainingSet.length) {
				this.testSet = new LabelledText[testSet.length];
				this.trainingSet = new LabelledText[trainingSet.length];
				//copies arrays
				ClassifierUtilities.copyArray(this.testSet, testSet);
				ClassifierUtilities.copyArray(this.trainingSet, trainingSet);
				this.categories = categories;
			}
			else {
				throw new IllegalArgumentException("Not every LabelledText has a valid category");
			}

	
	
	}



	/**
	 * Returns the trainingSet LabelledText array
	 * 
	 * @return LabelledText[] (trainingSet array of labelled texts)
	 */
	public LabelledText[] getTrainingSet() {

		return this.trainingSet;
	}

	/**
	 * Returns the testSet LabelledText array
	 * 
	 * @return LabelledText[] (testSet array of labelled texts)
	 */
	public LabelledText[] getTestSet() {

		return this.testSet;
	}

	/**
	 * Returns an object implementing the IProblemSet interface
	 * 
	 * @return IProblemSet (object implementing IProblemSet)
	 */
	public IProblemSet getCategories() {
		return this.categories;
	}

}
