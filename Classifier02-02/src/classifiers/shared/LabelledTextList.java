package classifiers.shared;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import classifier.interfaces.IProblemSet;
/**
 * LabelledTextList extends ArrayList<LabelledText>. 
 *  A LabelledTextList object has a private field IProblemSet
 *  LabelledTextList overrides the add(LabelledText text) and addAll(Collection<? extends
 *  LabelledText> texts) methods, but inherits the default versions of the other methods from the ArrayList class. 
 * @author Dania Bouhmidi
 *
 */
public class LabelledTextList extends ArrayList <LabelledText> {

	private IProblemSet categories;

	/**
	 * Constructor : Takes as input an object implementing IProblemSet 
	 * Sets the IProblemSet categories field
	 * @param categories : object of a class implementing the IProblemSet interface
	 */
	public LabelledTextList  ( IProblemSet categories) {
		this.categories = categories;

	}
	/**
	 * Returns the LabelledTextList's categories
	 * @return  object implementing IProblemSet representing the LabelledTextList's categories
	 */
	public  IProblemSet getProblemSet() {
		return this.categories;
	}

	/**
	 * Validates that the LabelledText to be added is valid (by checking if its category is contained inside
	 * the LabelledTextList's categories field)
	 * It it is valid,it add the LabelledText to the list
	 * If it is not valid,it throws an IllegalArgumentException
	 * @throws IllegalArgumentException : If the LabelledText object to be added has an invalid category
	 *  (a category not contained within the LabelledTextList's categories field)
	 * @return boolean : true when a LabelledText is added
	 */
	public boolean add (LabelledText text) {


		if (text.getCategory().contains(";") ) {

			if (validateCategory(text) == false) {
				throw new IllegalArgumentException("Invalid LabelledText Category");
			}

		}
		else {
			if ( ! this.categories.containsCategory(text.getCategory())){
				throw new IllegalArgumentException("Invalid LabelledText Category");
			}
		}

		return super.add(text);
	}

	/**
	 * Validates that the LabelledText Collection to be added is valid (by checking if its categories are contained inside
	 * the LabelledTextList's categories field)
	 * It it is valid,it add the LabelledText Collection to the list
	 * If it is not valid,it throws an IllegalArgumentException
	 * @throws IllegalArgumentException : If the LabelledText Collection to be added has an invalid category
	 *  (a category not contained within the LabelledTextList's categories field)
	 * @return boolean : true if this list is changed as a result of this method's call
	 */
	public boolean addAll(Collection<? extends LabelledText> texts) {
		for (LabelledText text : texts) {

			if (text.getCategory().contains(";")) {

				if (validateCategory(text) == false) {
					throw new IllegalArgumentException("Invalid LabelledText Category");
				}
			}


			else if ( ! this.categories.containsCategory(text.getCategory())){
				throw new IllegalArgumentException("Invalid LabelledText Category");
			}

		}

		return super.addAll(texts);

	}
	/**
	 * Confirms that the  categories within the LabelledText object is also within the IProblemSet.
	 * @param text : LabelledText object whose text contains multiple categories
	 * @return a boolean value indicating whether the LabelledText's categories are valid
	 */
	private boolean validateCategory (LabelledText text) {

		int contained =0;

		for (int i =0; i<text.getCategory().split(";").length; i++) {
			if ( this.categories.containsCategory( text.getCategory().split(";")[i])){
				contained++;
			}
		}
		if (contained == text.getCategory().split(";").length ) {
			return true;
		}
		return false;
	}
	/**
	 * Returns texts contained inside the LabelledTextList
	 * @return texts : texts contained inside the LabelledTextList
	 */
	public List<String> getTexts() {
		List<String> texts = new ArrayList<String>();
		for (LabelledText element : this) {
		texts.add(element.getText());
		}
		return texts;
	}








}



