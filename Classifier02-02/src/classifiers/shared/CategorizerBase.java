package classifiers.shared;
import java.util.ArrayList;
import java.util.List;

import classifier.interfaces.ICategorizer;
/**
 * CategorizerBase is an abstract class that implements ICategorizer
 * It only provides the categorizeAll method
 * @author Dania Bouhmidi
 *
 */
public abstract class CategorizerBase implements ICategorizer {

	
	/** 
	 * Iterates through the given List<String> and calls categorize(String) on each of its elements
	 * @param List<String> texts : a list of texts to be categorized
	 * @return List<String> : representing the categories returned by the categorize method for each of the list's element
	 */
	public  List<String> categorizeAll(List<String> texts) {
		List <String> categorizedList = new ArrayList<String>();
		for (String text : texts) {
			categorizedList.add(categorize(text));
		
		}
		
		return categorizedList;
		
	}
	
	
	
}
