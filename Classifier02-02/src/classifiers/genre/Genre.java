package classifiers.genre;
import classifier.interfaces.IProblemSet;
/*
 * @author Michelle Polonsky
 * 
 */
public class Genre implements IProblemSet{
  
  private String[] genres;
  private int numGenres;
  
  // constructor
  public Genre(String[] genres){ //initialize String[] with input
   
    this.genres = new String[genres.length];
    for(int i=0;i<genres.length;i++){
      this.genres[i] = genres[i];
    }
    this.numGenres = this.genres.length;
    
  }
  
  //returns a copy of a String[] that is stored in a private field
  public String[] getCategoryNames(){ 
    
   String[] copy = new String[this.genres.length];
    for(int i=0;i<genres.length;i++){
      copy[i] = this.genres[i];
    }
    return copy;
  }
  
  public int getNumGenres(){
	  return this.numGenres;
  }
  
}
