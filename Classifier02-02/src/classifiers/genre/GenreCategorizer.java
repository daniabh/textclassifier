package classifiers.genre;
import java.util.Arrays;

import classifier.interfaces.ICategorizer;
import classifier.utilities.ClassifierUtilities;
  
public class GenreCategorizer implements ICategorizer{
    /**
     * @author Michelle Polonsky
     * GenreCategorizer implements ICategorizer
     * A GenreCategorizer object takes as input a two-dimensional array of strings and a Genre object
     * It sorts each genre into a specific array
     * It categorizes a String by counting the most frequent genre in it 
     */
 /* PART 5 B) a. */
	private String[][] texts;
    private Genre genres;
    private String[] genrePerArray;
      
    //conStructor           jagged (non-rectangular)  Genre: String[] 
    public GenreCategorizer (String[][] texts, Genre genres){//assume all words in text relate to a genre
            
    //initialize properties
    	this.texts = texts;
        this.genres = genres;
        
        //store genres in one array 
        int genresLength = this.genres.getCategoryNames().length;//number of categories
        this.genrePerArray = new String[genresLength];
        for(int i=0; i<genresLength; i++){
         this.genrePerArray[i] =  this.genres.getCategoryNames()[i];
        }
                
        //join all words together
        for(int i=0; i<this.texts.length; i++) {
        this.texts[i][0] = String.join(" ", this.texts[i]);
        }
                        
        //take out punctuation, lowercase everything,
        for(int i=0; i<this.texts.length; i++){      
            this.texts[i][0] = this.texts[i][0].replaceAll("\\p{Punct}","");
            this.texts[i][0] = this.texts[i][0].toLowerCase();
        }//each genre has one(1) array of all the sentences
                                
        //split sentences into words
        String[][] splitWords = new String[genresLength][]; 
        for(int i=0; i<splitWords.length; i++){
          splitWords[i] = this.texts[i][0].split(" ");
        } 
             
        //sort in alphabetical order
        for(int i=0; i<splitWords.length; i++){
        	Arrays.sort(splitWords[i]);
        }
        
        this.texts = splitWords;
        
    }//end of constructor
      
/* PART 5 B) b. */ 
      
      //takes each word from text and categorizes it 
    public String categorize(String text){ // text = an entire document
            	
        text = text.replaceAll("\\p{Punct}","");
       String [] splitText = ClassifierUtilities.removePunctuationandCase(text.split(" "));
       splitText = ClassifierUtilities.removeDuplicates(splitText);
       ClassifierUtilities.alphabeticalSort(splitText);
        int[] wordsPerCat = new int[this.texts.length];
        
        for(int i=0; i<splitText.length; i++) { // every word in text
	        for(int j=0; j<4; j++) { // every category
	        	//binary search
	        	if (ClassifierUtilities.find(splitText[i], this.texts[j]) == 0) {
	        		wordsPerCat[j]++;
	        	}
	        }
        }
		      
	        	int maxLocation = ClassifierUtilities.findMax(wordsPerCat);
	        	int max = wordsPerCat[ClassifierUtilities.findMax(wordsPerCat)];
        if(max > (splitText.length)/2){//more than half the words
            return this.genrePerArray[maxLocation]; //return genre 
        }

        return null;
    }//end Categorize     
}//end of class
   