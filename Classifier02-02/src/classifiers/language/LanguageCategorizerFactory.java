package classifiers.language;

import classifiers.shared.DataCorpus;
/**
 * LanguageCategorizerFactory is used to create a LanguageCategorizer with a DataCorpus object's data
 * @author Dania Bouhmidi
 *
 */
public class LanguageCategorizerFactory {

	/**
	 * Takes as input a DataCorpus object 
	 * Uses the DataCorpus object to create a LanguageCategorizer object.
	 * @param DataCorpus object
	 * @return A LanguageCategorizer object created with the DataCorpus object's trainingSet and Language object
	 */
	public static LanguageCategorizer  learnLanguage (DataCorpus object) {
		LanguageCategorizer languageCategorizer = new LanguageCategorizer(object.getTrainingSet(), (Language) object.getCategories()); 
		return languageCategorizer;
	}

}

