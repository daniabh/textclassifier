package classifiers.language;
/**
 * 
 * @author Dania Bouhmidi
 * This Exception is thrown when a LanguageCategorizer object has a language different than French/English as input
 * This Exception is thrown when the text array's length and languages array's length of a LanguageCategorizer object are different
 */
public class InvalidLanguageException extends Exception {
  /**
   * Constructor: Constructs a new exception.
   */ 
  public InvalidLanguageException() {
    super(); 
  }
  /* Constructor: Constructs a new exception and requires a message as input.
   * @param message (specified detail message)
   */ 
  public InvalidLanguageException(String message) {
    super(message); 
  }
}
