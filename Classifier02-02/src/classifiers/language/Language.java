package classifiers.language;
import classifier.interfaces.IProblemSet;
import classifier.utilities.ClassifierUtilities;

/**
 * @author Dania Bouhmidi
 * Language is a class that implements the IProblemSet interface
 */
public class Language implements IProblemSet {
private  String [] categoryNames;
 public String [] getCategoryNames(){
  return this.categoryNames;
 }
public Language (String [] categoryNames) {
	this.categoryNames = new String [categoryNames.length];
	ClassifierUtilities.copyArray(this.categoryNames, categoryNames);
}
}

