package classifier.interfaces;
/**
 * @author Dania Bouhmidi
 * The IProblemSet interface has a getCategoryNames method, 
 * It has a containsCategory method that takes as input a String category
 * It has a isUnique method that returns a boolean value
 * An object implementing this interface must provide these methods.
 */
public interface IProblemSet {
public String [] getCategoryNames();

boolean containsCategory(String category);

boolean isUnique();

}
