package classifier.interfaces;

import java.util.List;

/**
 * @author Dania Bouhmidi
 * The ICategorizer interface has a categorize method that takes as input a String and also returns a String,
 * It also has a categorizeAll method that takes as input a List<String> and also returns a List<String>
 * An object implementing this interface must provide these methods
 */
public interface ICategorizer {

public String categorize(String text);
public List<String> categorizeAll(List<String> texts);
}

