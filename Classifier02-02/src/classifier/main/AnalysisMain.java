package classifier.main;

import java.io.IOException;

import java.util.List;
import java.util.Scanner;
import classifier.interfaces.ICategorizer;
import classifier.interfaces.IDataSetSerializer;
import classifiers.shared.LabelledText;
import classifiers.shared.LabelledTextList;
import classifiers.shared.MultiCategorizer;
import classifiers.shared.SingleCategorizer;
import fileio.tweets.TweetSerializer;
import fileio.languages.WiliLanguageSerializer;
import fileio.genres.*;
/**
 * @author Dania Bouhmidi and Michelle Polonsky
 * AnalysisMain is used to run one of the categorizers (SingleCategorizer/MultiCategorizer)
 * on a different dataset than its training dataset, to produce more labelled data. 
 *
 */
public class AnalysisMain {

	private static Scanner reader = new Scanner (System.in);
	private static ICategorizer categorizer;
	private static LabelledTextList trainLabelledList;
	private static IDataSetSerializer trainingSerializer;
	private static LabelledTextList results;
	private static IDataSetSerializer testSerializer;
	private static LabelledTextList testLabelledList;
	private static int chosenTrainingDataSet =0;

	public static void main(String[] args) {
		chooseTrainingDataSet();
		chooseTestDataSet();
		chooseOutputFolder();
	}
	/**
	 * Asks the user for the root path of a folder containing a dataset as input
	 * Presents a  text menu to the user where they can specify which categorizer
	 * this folder should be used to create an ICategorizer
	 * return @void
	 */
	public static void chooseTrainingDataSet() {
		System.out.println("Pleaser enter a root path containing the file(s) and/or directories of one of the datasets that will be used as a training dataset");
		String trainingDataSetPath = reader.nextLine();
		System.out.println("Please enter : " + "\r\n"+"1 for Wili Set"+
				"\r\n"+"2 for Bbc set"+ "\r\n"+"3 for Tweet set");	
		chosenTrainingDataSet = reader.nextInt();
		try {
			switch (chosenTrainingDataSet) {
			case 1:
				trainingSerializer  = new WiliLanguageSerializer(trainingDataSetPath);
				trainLabelledList = new LabelledTextList(trainingSerializer.load().getProblemSet());
				trainLabelledList.addAll((trainingSerializer.load()));
				categorizer = new SingleCategorizer (trainLabelledList, true);

				break;
				
			case 2:
				trainingSerializer  = new BbcGenreSerializer(trainingDataSetPath);
				trainLabelledList = new LabelledTextList(trainingSerializer.load().getProblemSet());
				trainLabelledList.addAll((trainingSerializer.load()));
				categorizer = new SingleCategorizer (trainLabelledList, true);
				
				break;
			
			case 3:
				trainingSerializer  = new TweetSerializer(trainingDataSetPath);
				trainLabelledList = new LabelledTextList(trainingSerializer.load().getProblemSet());
				trainLabelledList.addAll((trainingSerializer.load()));
				categorizer = new MultiCategorizer (trainLabelledList, true);

				break;
			}
		}
		catch (IOException e) {

			System.out.println(trainingDataSetPath + " is an invalid path");
			System.exit(0);
		}
	}
	
	public static void chooseTestDataSet() {
		System.out.println("Pleaser enter a root path containing the file(s) and/or directories of one of the datasets that will be used as a testing dataset");
		String testDataSetPath = reader.next();
		System.out.println("Please enter : " + "\r\n"+"1 for Wili Set"+
				"\r\n"+"2 for Bbc set"+ "\r\n"+"3 for Tweet set");	
		List <String> categorizedList;
		results = new LabelledTextList(trainLabelledList.getProblemSet() );
		int chosenTestDataSet = reader.nextInt();
		try {
			switch ( chosenTestDataSet) {
			
			case 1:

				testSerializer  = new WiliLanguageSerializer(testDataSetPath);
				testLabelledList = new LabelledTextList(testSerializer.load().getProblemSet());
				testLabelledList.addAll((testSerializer.load()));
				 categorizedList = categorizer.categorizeAll(testLabelledList.getTexts());
				for (int i =0; i<testLabelledList.size(); i++) {
					//only keep matches
					if (categorizedList.get(i) != null) {
						results.add(  new LabelledText (categorizedList.get(i),testLabelledList.get(i).getText()));
					}
				}
				break;
			
			case 2:
				
				testSerializer  = new BbcGenreSerializer(testDataSetPath);
				testLabelledList = new LabelledTextList(testSerializer.load().getProblemSet());
				testLabelledList.addAll((testSerializer.load()));
				 categorizedList = categorizer.categorizeAll(testLabelledList.getTexts());
				for (int i =0; i<testLabelledList.size(); i++) {
					//only keep matches
					if (categorizedList.get(i) != null) {
						results.add(  new LabelledText (categorizedList.get(i),testLabelledList.get(i).getText()));
					}
				}
				break;
				
			case 3:

				testSerializer  = new TweetSerializer(testDataSetPath);
				testLabelledList = new LabelledTextList(testSerializer.load().getProblemSet());
				testLabelledList.addAll((testSerializer.load()));
				categorizedList = categorizer.categorizeAll(testLabelledList.getTexts());
				for (int i =0; i<testLabelledList.size(); i++) {
					//only keep matches
					if (categorizedList.get(i) != null) {
						results.add(  new LabelledText (categorizedList.get(i),testLabelledList.get(i).getText()));
					}

				}

				break;

			}
		}
		catch (IOException e) {

			System.out.println(testDataSetPath + " is an invalid path");
			System.exit(0);
		}

	}
	/**
	 * chooseOutputFolder allows the user to choose a folder to store the results of the categorizing algorithm
	 * If the chosen trainingSet is a Tweet dataset, the results will be appended to the TweetSerializer's folder
	 * @return void
	 */
	public static void chooseOutputFolder() {
		String resultsPath = null;
		IDataSetSerializer resultsFile;
		switch  (chosenTrainingDataSet) {
		case 1:
			try {
				System.out.println("Pleaser enter a root path to store the results of the categorizing algorithm");
				resultsPath = reader.next();
				resultsFile = new WiliLanguageSerializer(resultsPath);
				resultsFile.save(results);
			}
			catch (IOException e) {
				System.out.println(resultsPath + " is an invalid path");
				System.exit(0);
			}
			break;
			
		case 2:
			try {
				System.out.println("Pleaser enter a root path to store the results of the categorizing algorithm");
				resultsPath = reader.next();
				resultsFile = new BbcGenreSerializer(resultsPath);
				resultsFile.save(results);
			}
			catch (IOException e) {
				System.out.println(resultsPath + " is an invalid path");
				System.exit(0);
			}
			break;	
			
		case 3:
			//results are appended to the folder given as input to the trainSerializer object
			try {
				trainingSerializer.save(results);

			}
			catch (IOException e) {
				System.out.println(((WiliLanguageSerializer)trainingSerializer).getFolderName()  + " is an invalid path");
				System.exit(0);
			}

			System.out.println("Results are appended to the trainingDataSet folder");
			break;
		}
	}
}