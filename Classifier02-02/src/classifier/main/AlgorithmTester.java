package classifier.main;
import java.io.IOException;
import java.nio.file.*;
import classifier.interfaces.ICategorizer;
import classifiers.genre.GenreCategorizer;
import classifiers.language.*;
import fio.languages.WiliParser;
import fileio.genres.*;
import classifiers.shared.*;
/**
 * AlgorithmTester is used to train and test the 
 * categorizing algorithm of language categorizers and genre categorizers
 * created with a DataCorpus object's data
 * @author Dania Bouhmidi and Michelle Polonsky
 *
 */
public class AlgorithmTester {
	private static LanguageCategorizer languageCategorizer;
	private static DataCorpus languageDataCorpus;
	private static GenreCategorizer genreCategorizer;
	private static DataCorpus genreDataCorpus;


	public static void main(String[] args)  {
		Path workingDirectory=Paths.get(".").toAbsolutePath();
		try {
		createLanguageData(workingDirectory+"\\src\\fio\\languages\\wili-2018-small");
			double accuracy = test(languageDataCorpus.getTestSet(),languageCategorizer);
			System.out.println("Language Categorizer Accuracy : "+accuracy +"%");
		
		createGenreData(workingDirectory+"\\src\\fileio\\genres\\bbcsmall");
		    accuracy = test(genreDataCorpus.getTestSet(), genreCategorizer);
		    System.out.println("Genre Categorizer Accuracy : "+accuracy +"%");
		
		}
		catch (IOException e ){
			System.out.println("Invalid path : " + e);

		}
		catch ( IllegalArgumentException e){
			System.out.println("Invalid path : " + e);

		}
	}


	/**
	 * Takes as input an array of labelledTexts and a ICategorizer categorizer,
	 * Calls he categorize method by passing the text only
	 * Compares the result return by the categorize() with the result stored within the labelledTexts array
	 * Returns the proportion of times the algorithm was correct
	 * @param labelledTexts (labelledTexts [])
	 * @param categorizer (ICategorizer categorizer)
	 * @return double value representing the accuracy of the categorizing algorithm

	 */
	public static double test(LabelledText[] labelledTexts, ICategorizer categorizer) throws IOException {

		int labelledRight=0;
		
		for(int i =0; i<labelledTexts.length; i++) {
		
			if (labelledTexts[i].getCategory().equals (categorizer.categorize(labelledTexts[i].getText()))) {
		
				labelledRight++;
			}
		}

		double accuracy =  ((double)labelledRight/(double)labelledTexts.length)*100;
		return accuracy;
	}



	/**
	 * Creates a LanguageCategorizer object
	 * @param foldername to create a DataCorpus object and use it to create a LanguageCategorizer object
	 * @throws IOException (If foldername is not found)
	 * @throws IOException (If foldername does not contain necessary files)
	 * @throws IOException (If one or both pairs of X/Y files do not have the same number of lines)
	 */
	private static void createLanguageData(String foldername) throws IOException {
		WiliParser.generateDataCorpus(foldername);
		languageDataCorpus =  WiliParser.generateDataCorpus(foldername);
		languageCategorizer = LanguageCategorizerFactory.learnLanguage(languageDataCorpus);

	}
	
	/**
	 * Creates a GenreCategorizer object
	 * @param foldername is a path to a directory
	 * @throws IOException (If foldername is not found)
	 * @throws IOException (If foldername does not contain necessary files)
	 */
	private static void createGenreData(String foldername) throws IOException{
	    BbcParser.generateDataCorpus(foldername);
	    genreDataCorpus = BbcParser.generateDataCorpus(foldername);
	}



}
