package classifier.main;
import java.util.Scanner;
import classifier.interfaces.ICategorizer;
import classifiers.language.InvalidLanguageException;
import classifiers.language.Language;
import classifiers.language.LanguageCategorizer;
import classifiers.genre.Genre;
import classifiers.genre.GenreCategorizer;

/**
 * @author Michelle Polonsky and Dania Bouhmidi
 * ClassifierMain is used to instantiate a LanguageCategorizer and a GenreCategorizer object inside an ICategorizer array
 * It asks the user for a sentence and each categorizer object categorizes it
 */
public class ClassifierMain {
/**
 * 
 */
 private static Scanner reader;

 /**
  * @param args
  * @return void
  */
 /*public static void main(String[] args) throws InvalidLanguageException{
  reader = new Scanner (System.in);
  ICategorizer [] categorizers = new ICategorizer[2];
  String [] sentences = {"Bonjour","hi there there","Dan ne peux pas Francais"};
  String [] languages = {"French", "English","French"};
  Language language = new Language();
  categorizers [0] = new LanguageCategorizer(sentences, languages, language);
  
  
  String[][] texts = 
  {{"country president","Prime Minister","win!!!!!"}, 
  {"hockey Olympics","win"}  , 
  {"painting painting painting", "Painting"}
  };
  String [] choiceofgenre = {"news","sports","arts"};
  Genre genres = new Genre(choiceofgenre);
  categorizers [1] = new GenreCategorizer(texts,genres);
  
  
  System.out.println("Enter a sentence to be categorized");
  String text = reader.nextLine();
  for (ICategorizer classifier : categorizers) {
   System.out.println("Categorized:  " +classifier.categorize(text));
  }
 }
*/
}
