package fileio.genres;

import classifiers.genre.*;
import classifiers.shared.*;
import java.io.*;
import java.nio.*;
import java.nio.file.*;
import java.util.*;
import classifier.interfaces.*;

/**
 * 
 * @author Michelle Polonsky
 * BbcParser generates a DataCorpus and a GenreCategorizer
 *
 */

public class BbcParser {
	
	/**
	 * Generates a DataCorpus based on a given folder
	 * @param foldername is a path to a directory 
	 * @return a DataCorpus representing the text files used to train the algorithm and the text used to test the class
	 * @throws IOException
	 */
								//path to folder (that contain multiple folders indicating genre names)
	public static DataCorpus generateDataCorpus(String foldername) throws IOException{
		File dir = new File(foldername);
		Genre genres = getBbcGenres(foldername);
		int numOfFiles = totalNumTextDoc(foldername);
		int percent80 = numOfFiles*80/100;
		int percent20 = numOfFiles - percent80;//won't always exactly be 20% depending on number of files
		LabelledText[] trainingSet = new LabelledText[percent80];
		LabelledText[] testSet = new LabelledText[percent20];

		int files = 0;
		int genreNum = 0;
		int nextDir = 0;//directory at which start storing 20%
		int nextFile = 0;//file at which start storing 20%
		//stores first 80% of files  in trainingSet
			for(int i=0; i<dir.list().length; i++) {
				File genreFile = new File(dir+"\\"+dir.list()[i]);//creates a new file for a genre every time loop restarts
				if(!genreFile.isDirectory()){
			        i++;
			        genreFile = new File(dir+"\\"+dir.list()[i]);
			    }
				for(int j=0; j<genreFile.list().length; j++) {
			        Scanner input = new Scanner(new File(genreFile+"\\"+genreFile.list()[j]));
			        String text = "";
			        while (input.hasNextLine()){
			          text = text+" "+input.nextLine();
			        }
					trainingSet[files] = new LabelledText(genres.getCategoryNames()[genreNum], text);
					files++;
					if(files == percent80) {//stops when 80% of files have been stored
						nextFile = j;
						break;
					}
				    input.close();
			    }
				if(files == percent80) {//stops when 80% of files have been stored
					nextDir = i;
					break;
				}
				genreNum++;
			}
			
			files = 0;
					
		//stores last 20% of files in testSet (stores all files left to store)
			for(int i=nextDir; i<dir.list().length; i++) {//goes through each file inside bbc folder
				File genreFile = new File(dir+"\\"+dir.list()[i]);//creates a new file for a genre every time loop restarts
				if(!genreFile.isDirectory()){//if file isn't a folder
			        i++;// go to next file
			        genreFile = new File(dir+"\\"+dir.list()[i]);
			    }
				for(int j=nextFile; j<genreFile.list().length; j++) {
			        Scanner input = new Scanner(new File(genreFile+"\\"+genreFile.list()[j]));
			        String text = "";
			        while (input.hasNextLine()){
			          text = text+" "+input.nextLine();
			        }
					testSet[files] = new LabelledText(genres.getCategoryNames()[genreNum], text);
					files++;
					if(files == percent20) {//stops when 20% of files have been stored (in case something goes wrong)
						break;
					}
				    input.close();
			    }
				if(files == percent20) {//stops when 20% of files have been stored (just as a precaution)s
					break;
				}
				genreNum++;
			}
						
		DataCorpus dataCorpus = new DataCorpus(trainingSet, testSet, genres); 
			
		return dataCorpus;
		
	}
	
	/**
	 * Generates a GenreCategorizer based on a given folder
	 * @param foldername is a path to a directory
	 * @return a GenreCategorizer to categorize a text file inside a category folder
	 * @throws IOException is thrown if the given foldername is not an existing directory
	 */
													//path to bbcsmall
	public GenreCategorizer generateGenreCategorizer(String foldername) throws IOException{
			
		File directory = new File(foldername);
		Genre genres = getBbcGenres(foldername);
		String[][] texts = new String[directory.list().length][5];//[num of genres+readme.txt][], only works with 5 text files per genre

		for(int i=0; i<texts.length; i++) {
			File genreFile = new File(directory+"\\"+directory.list()[i]);
			if(!genreFile.isDirectory()){
		        i++;
		        genreFile = new File(directory+"\\"+directory.list()[i]);
		    }
			for(int j=0; j<genreFile.list().length; j++) {
		        Scanner input = new Scanner(new File(genreFile+"\\"+genreFile.list()[j]));
		        texts[i][j] = "";
		        while (input.hasNextLine()){
		          texts[i][j] = texts[i][j]+" "+input.nextLine();
		        }
		        input.close();
		      }
		}
		
		GenreCategorizer folder = new GenreCategorizer(texts, genres);//will have 1 (one) more array than needed
		return folder;
	}
	
	/**
	 * Returns all the genres in BBC articles
	 * @param foldername is a path to a directory
	 * @return a Genre with all the genres in the given folder
	 * @throws IOException is thrown if the given the foldername is not an existing directory
	 */
	public static Genre getBbcGenres(String foldername) throws IOException{
		
		File directory = new File(foldername); 
		
		FileFilter dirFilter = new FileFilter() { // stores all files that are directories
			public boolean accept(File file) {
				return file.isDirectory();
			}
		};
		
		File[] genresFile = directory.listFiles(dirFilter);
		
		String[] genresString = new String[genresFile.length];
	    for(int i=0; i<genresString.length; i++){
	      genresString[i] = genresFile[i].getName();
	    }	    
	    
		Genre genres = new Genre(genresString);
	    
	    return genres;
	}/* snippet of code can be found on
	avajava.com/tutorials/lessons/how-do-i-use-a-filefilter-to-display-only-the-directories-within-a-directory.html*/
	
	/**
	 * Returns the total number of text files inside the category folders
	 * @param foldername is a path to a directory
	 * @return the total number of articles 
	 */
	public static int totalNumTextDoc(String foldername) {
		
		File dir = new File(foldername);
		int total = 0;
		
		for(int i=0; i<dir.list().length; i++) {
			File genreFile = new File(dir+"\\"+dir.list()[i]);
			if(genreFile.isDirectory()){//if file is a directory ( if it's a genre folder)
		        total += genreFile.list().length; // add number of text files in it to total
		    }
		}
		return total;
	}
	
}
