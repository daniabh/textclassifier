package fileio.languages;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import classifier.interfaces.IDataSetSerializer;
import classifiers.shared.LabelledText;
import classifiers.shared.LabelledTextList;
import classifiers.shared.SingleProblemSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set; 
/**
 * WiliLanguageSerializer implements IDataSetSerializer. 
 * The purpose of this class is to create a LabelledTextList based on a Wili dataset. 
 * This LabelledTextList object can then be used to create a SingleCategorizer or MultipleCategorizer.
 * @author Dania Bouhmidi
 *
 */
public class WiliLanguageSerializer implements IDataSetSerializer  {
	private String foldername;

	private static HashMap<String, String> languageCodes= new HashMap<String, String>();
	private  static HashMap<String, String> languages= new HashMap<String, String>();
	/**
	 * Constructor : Creates a WiliLanguageSerializer with the foldername given as input
	 * @param foldername : folder to load from or save to
	 * @throws IOException (if object's foldername does not exists)
	 */
	public WiliLanguageSerializer(String foldername) throws IOException {
		this.foldername = foldername; 

	}

	/**
	 * Returns the foldername of the object
	 * @return foldername : folder that was used to create this WiliLangugaeSerializer object
	 */
	public String getFolderName() {
		return this.foldername;
	}

	/**
	 * Creates a LabelledTextList   IProblemSet field  set based on the categories contained inside the y_ files
	 * Creates a LabelledTextList with the lines contained inside x_train and y_train
	 * Converts language codes to languages in both the LabelledTextList and the IProblemSet object
	 * @return a LabelledTextList created from x/y files inside the fodlername field whose labels are converted from language codes to languages
	 * @throws IOException (if object's foldername does not exists) 
	 */
	@Override
	public LabelledTextList load() throws IOException {


		setLanguageCodes(foldername+"\\labels.csv");
		String [] categoryNames = setCategories(foldername+"\\y_train.txt") ;
		for (int i =0; i<categoryNames.length; i++) { 
			//convert language codes to languages
			categoryNames[i] = languages.get(categoryNames[i]);
		}

		SingleProblemSet categories = new SingleProblemSet(categoryNames,false);
		LabelledTextList train =  new LabelledTextList(categories);
		LabelledTextList list  = (setLabelledTextList(foldername+"\\x_train.txt", foldername+"\\y_train.txt"));

		//convert all language codes to languages
		for (int i =0; i<list.size(); i++) {
			LabelledText element = new LabelledText(languages.get(list.get(i).getCategory()  ),list.get(i).getText());
			train.add(element);
		}


		return train;

	}
	/**
	 * Takes as input a LabelledTextList
	 * Produces three files x_train.txt, y_train.txt, and labels.csv. These files are then
	 *stored in the folder that was given as input to the constructor. If this folder
	 * already exists, then your code it throws an IOException. 
	 * @return void
	 * @throws IOException (if object's foldername already exists) 
	 */
	@Override
	public void save(LabelledTextList list) throws IOException {

		Path path = Paths.get(this.foldername);
		if (Files.isDirectory(path)) {
			throw new IOException ("Folder already exists");
		}
		else {
			File dir = new File(this.foldername);
			dir.mkdir();
		}

		ArrayList <String> y_train = new ArrayList <String>();
		ArrayList <String> x_train = new ArrayList<String>();
		//add label codes
		for (LabelledText element : list) {
			//if text contains multiple languages :
			// add text multiple times, with different language codes
			if (element.getCategory().contains(";")) {
				for (int i =0; i<element.getCategory().split(";").length; i++ ) {
					y_train.add((languageCodes.get(element.getCategory().split(";")[i] )));
					x_train.add(element.getText());
				}
			}
			else {
				y_train.add((languageCodes.get(element.getCategory())));
				x_train.add(element.getText());
			}
		}


		ArrayList <String> labels= new ArrayList <String>();
		labels.add("Language Code"+";"+"Language");
		for ( Entry<String, String> element : languageCodes.entrySet()) {
			labels.add(element.getValue() + ";"+ element.getKey()  );
		}

		Path file1 = Paths.get(this.foldername+"//y_train.txt");
		Path file2= Paths.get(this.foldername+"//x_train.txt");
		Path file3= Paths.get(this.foldername+"//labels.csv");
		Files.write(file1, y_train);  
		Files.write (file2,x_train);

		Files.write (file3,labels);



	}

	/**
	 * Takes as input a String representing a file containing text labels
	 *  Initializes the categories field with those labels (without duplicates)
	 * @param labelfile (text file containing labels)
	 * @throws IOException (if file not found)
	 */
	private  String [] setCategories(String labelfile) throws IOException {
		Path path1= Paths.get(labelfile);

		ArrayList <String> allLabels = new ArrayList <String>();

		//stores all labels from input file(including duplicates)
		allLabels.addAll(Files.readAllLines(path1));

		//removes duplicate labels
		Set<String> set = new HashSet<>(allLabels);
		allLabels.clear();
		allLabels.addAll(set);
		ArrayList <String> labels = new ArrayList<String>();
		labels.addAll(set);


		//initializes array with right size and sets values at each index
		String [] categories = new String[allLabels.size()];

		for (int i =0; i<labels.size(); i++) {
			categories[i] = labels.get(i).toString();
		}

		return categories;

	}





	/**
	 * Takes as input a file containing texts and a file containing the labels of each of these texts
	 * Creates a LabelledText from the data inside the two input files
	 * @param textfile (containing texts)
	 * @param labelfile (containing labels for the texts)
	 * @return LabelledTextList created from the 2 input files
	 * @throws IOException (if folder is not found)
	 */
	private LabelledTextList setLabelledTextList(String textfile, String labelfile) throws IOException {

		Path sentences= Paths.get(textfile);
		Path labels= Paths.get(labelfile);
		List<String> text = Files.readAllLines(sentences);
		List<String> language = Files.readAllLines(labels);
		LabelledText[] labelledtexts = new LabelledText[text.size()];
		//convert language codes to language
		for (int i =0; i<text.size(); i++) {

			labelledtexts[i] = new LabelledText (language.get(i),text.get(i));


		}

		String [] categoryNames = setCategories(foldername+"\\y_train.txt") ;
		SingleProblemSet categories = new SingleProblemSet(categoryNames,true);
		LabelledTextList labelledTextList = new LabelledTextList (categories);

		for (int i =0; i<labelledtexts.length; i++) {

			labelledTextList.add(labelledtexts[i]);

		}

		return labelledTextList;
	} 


	/**
	 * Sets the languageCodes and languages hashmap fields
	 * by storing the first column and second column of the labels.csv file as  keys/values
	 * @param labelfile
	 * @return void
	 * @throws IOException (if object's foldername does not exists) 
	 */
	private  void setLanguageCodes(String labelfile) throws IOException {

		Path path1= Paths.get(labelfile);

		ArrayList <String> allLabels = new ArrayList <String>();

		//reads all the lines from the labels.csv file
		allLabels.addAll(Files.readAllLines(path1));

		ArrayList <String> firstColumn = new ArrayList <String>();
		ArrayList <String> secondColumn = new ArrayList <String>();

		//label info array for each label
		//iterate through allLabels  content and store two first Strings delimited by ";" 
		for (int i =0; i<allLabels.size(); i++) { 
			String[] labelinfoArrayParsed = allLabels.get(i).split(";");
			firstColumn.add(labelinfoArrayParsed[0]); //Language code
			secondColumn.add(labelinfoArrayParsed[1]); //Language

		}


		for (int i =0; i<firstColumn.size(); i++) {
			languages.put(firstColumn.get(i),secondColumn.get(i));
			languageCodes.put(secondColumn.get(i),firstColumn.get(i));
		}


	}



}
