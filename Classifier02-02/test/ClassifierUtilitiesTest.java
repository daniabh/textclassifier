import java.util.Arrays;

import classifier.utilities.ClassifierUtilities;

public class ClassifierUtilitiesTest {
	/**
	 * @author Dania Bouhmidi
	 * @version 2019-09-27 
	 * ClassifierUtilitiesTest serves as a Test for the ClassifierUtilities class
	 */
	public static void main(String[] args) {
		removePunctuationandCaseTest();
		removeDuplicatesTest();
		getArrayasStringTest();
		alphabeticalSortTest();
		findMaxTest();
		findTest();
		addArraysTest();
		copyArrayTest();

	}

	/**
	 * Tests the removePunctuationandCase method
	 * @return void
	 */
	public static void removePunctuationandCaseTest() {

		String[] test = { "Bonjour!", "tout le monde!", "IL FAIT FROID" };
		String[] expected = { "bonjour", "tout le monde", "il fait froid" };
		String[] output = ClassifierUtilities.removePunctuationandCase(test);

		String[] test2 = { "Ceci", "IL EST QUATRE HEURES!", "IL PLEUT" };
		String[] expected2 = { "ceci", "il est quatre heures", "il pleut" };
		String[] output2 = ClassifierUtilities.removePunctuationandCase(test2);

		if (expected[0].equals(output[0]) && expected[1].equals(output[1]) && expected[2].equals(output[2])
				&& expected2[0].equals(output2[0]) && expected2[1].equals(output2[1])
				&& expected2[2].equals(output2[2])) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}
	}

	/**
	 * Tests the removeDuplicates method
	 * @return void
	 */
	public static void removeDuplicatesTest() {
		int successone = 0;
		int successtwo = 0;
		String[] test = { "A", "test", "test", "method", "for duplicates" };
		String[] expected = { "A", "test", "method", "for duplicates" };
		String[] output = ClassifierUtilities.removeDuplicates(test);

		String[] test2 = { "duplicates", "duplicates", "hello world", "hello world" };
		String[] expected2 = { "duplicates", "hello world" };
		String[] output2 = ClassifierUtilities.removeDuplicates(test2);
		for (int i = 0; i < expected.length; i++) {
			if (output[i].equals(expected[i])) {
				successone++;
			}
		}
		for (int i = 0; i < expected2.length; i++) {
			if (output2[i].equals(expected2[i])) {
				successtwo++;
			}
		}
		if (successone == expected.length && successtwo == expected2.length) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
			System.out.println(Arrays.toString(ClassifierUtilities.removeDuplicates(test)));
			System.out.println(Arrays.toString(ClassifierUtilities.removeDuplicates(test2)));
		}
	}

	/**
	 * Tests the getArrayasString method
	 * @return void
	 */
	public static void getArrayasStringTest() {
		String[] test = { "We are", "testing the getArray method", "right now" };
		String expected = "We are testing the getArray method right now";
		String output = ClassifierUtilities.getArrayasString(test);
		String[] test2 = { "I", "return arrays", "as strings when I am called" };
		String expected2 = "I return arrays as strings when I am called";
		String output2 = ClassifierUtilities.getArrayasString(test2);
		if (output.equals(expected) && output2.equals(expected2)) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}
	}

	/**
	 * Tests the alphabeticalSort method
	 * @return void
	 */
	public static void alphabeticalSortTest() {
		int sortedOne = 0;
		int sortedTwo = 0;
		String[] test = { "RIGHT NOW", "CLASS", "BOOKS" };
		String[] expected = { "BOOKS", "CLASS", "RIGHT NOW" };
		ClassifierUtilities.alphabeticalSort(test);
		String[] test2 = { "letters", "green", "blue", "air" };
		String[] expected2 = { "air", "blue", "green", "letters" };
		ClassifierUtilities.alphabeticalSort(test2);
		for (int i = 0; i < test.length; i++) {

			if (test[i].equals(expected[i])) {
				sortedOne++;
			}
		}

		for (int i = 0; i < test2.length; i++) {

			if (test2[i].equals(expected2[i])) {
				sortedTwo++;
			}
		}
		if (sortedOne == test.length && sortedTwo == test2.length) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}

	}

	/**
	 * Tests the findMax method
	 * @return void
	 */
	public static void findMaxTest() {
		int[] counts = { 1, 2, 50, 7, 8, 9, 10 };
		int[] counts2 = { 100, 2, 1, 7, 8, 9, 10 };
		int[] counts3 = { 1, 2, 50, 70 };
		if (ClassifierUtilities.findMax(counts) == 2 && ClassifierUtilities.findMax(counts2) == 0
				&& ClassifierUtilities.findMax(counts3) == 3) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}
	}

	/**
	 * Tests the find method
	 * @return void
	 */
	public static void findTest() {
		int success = 0;
		String test = "right left ,middle";
		String[] document = test.split(" ");
		document = ClassifierUtilities.removePunctuationandCase(document);
		document = ClassifierUtilities.removeDuplicates(document);
		ClassifierUtilities.alphabeticalSort(document);
		for (int i = 0; i < document.length; i++) {
			if (ClassifierUtilities.find(document[i], document) == 0) {
				success++;
			}
		}
		if (success == document.length) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}

	}

	/**
	 * Tests the copyArray method
	 * @return void
	 */
	public static void copyArrayTest() {
		int success = 0;
		int success2 = 0;
		String[] two = { "1", "2", "3", "4" };
		String[] one = new String[two.length];
		ClassifierUtilities.copyArray(one, two);

		String[] four = { "three", "two", "one", "zero" };
		String[] three = new String[four.length];
		ClassifierUtilities.copyArray(three, four);

		for (int i = 0; i < two.length; i++) {
			if (one[i].equals(two[i])) {
				success++;
			}
		}

		for (int i = 0; i < four.length; i++) {
			if (three[i].equals(four[i])) {
				success2++;
			}
		}
		if (success == two.length && success2 == four.length) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}
	}

	/**
	 * Tests the addArrays method
	 * @return void
	 */
	public static void addArraysTest() {
		int success = 0;
		int success2 = 0;
		String[] first = { "One", "Two", "Three" };
		String[] second = { "Four", "Five", "Six" };
		String[] expected = { "One", "Two", "Three", "Four", "Five", "Six" };
		String[] output = ClassifierUtilities.addArrays(first, second);
		for (int i = 0; i < expected.length; i++) {
			if (expected[i].equals(output[i])) {
				success++;
			}
		}

		String[] first2 = { "un deux trois", "quatre", "cinq" };
		String[] second2 = { "six", "sept", "huit" };
		String[] expected2 = { "un deux trois", "quatre", "cinq", "six", "sept", "huit" };
		String[] output2 = ClassifierUtilities.addArrays(first2, second2);
		for (int i = 0; i < expected2.length; i++) {
			if (expected2[i].equals(output2[i])) {
				success2++;
			}
		}

		if (success == expected.length && success2 == expected2.length) {
			System.out.println("PASS");
		}
	}
}
