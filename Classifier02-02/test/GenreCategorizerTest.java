import classifiers.genre.*;

/**
 * 
 * @author Michelle Polonsky
 * @version 2019-10-29
 * GenreCategorizerTest is a test class to test all the possible inputs in GenreCategorizer
 *
 */

public class GenreCategorizerTest{
  
 /**
  * Main method that tests out the possible GenreCategorizer
  * @param args
  */
 
 //@Test
	public static void main(String[]args){
    
	  //GenreCategorizer (String[][] texts, Genre genres)
	  String[][] texts = { {"Report scandal", "News scoop", "President Obama", "Traffic accident", "Murder!!!", "Police" },
	        {"Picasso Painting", "drawing sketch", "Red paint", "Signing", "Dancing ShOw!!", "colors","music", "art"},
	        {"Hockey Finals", "basketball game", "GOAL!!!!", "Scoring points", "victory", "team wins", "rugby", "sport"},
	        {"Statistics results", "thesis", "research", "scientists", "conclusion", "analyzing graphs", "CLIMATE! CHANGE!"} };
	  String[] allGenres = {"news", "art", "sport", "research"};
	  Genre genres = new Genre(allGenres);
	  GenreCategorizer test = new GenreCategorizer(texts, genres);
	  
	  
	  // String categorize(String text)
	  String test1 = "news report painting Thesis ReSearch scientists!! game"; // category: null
	  String test2 = "Game traffic murder police basketball Obama"; // category: news 
	  String test3 = "THESIS RESEARCH CONCLUSION hockey game"; // category: research
	  String test4 = "finals Goal!!!! red victory president GRAPHS TEAM"; //cat: sports
	  String test5 = "PICASSO CliMaTe cHAngE paint signing police colors !analyzing! music"; //cat: art
	  String test6 = "report, sketch, points TheSIS ";// cat: null 
	  String test7 = "this has nothing to do with anything. paint."; //cat: null
	  
	  
	  String category1 = test.categorize(test1);
	  System.out.print("test1");
	  if(category1 == null) {
		  System.out.println(" pass");
	  }
	  else {
		  System.out.println(" fail");
	  }//if returns null: pass
	
	  
	  String category2 = test.categorize(test2);
	  System.out.print("test2");
	  if(category2 == null) {
		  System.out.println(" fail");
	  }
	  else if(category2.equals("news")){
		  System.out.println(" pass");
	  }//if returns news: pass
	  else {
		  System.out.println(" fail");
	  }
	
	  
	  String category3 = test.categorize(test3);
	  System.out.print("test3");
	  if(category3 == null) {
		  System.out.println(" fail");
	  }
	  else if(category3.equals("research")){
		  System.out.println(" pass");
	  }//if returns research: pass
	  else {
		  System.out.println(" fail");
	  }
	
	  
	  String category4 = test.categorize(test4);
	  System.out.print("test4");
	  if(category4 == null) {
		  System.out.println(" fail");
	  }
	  else if(category4.equals("sport")){
		  System.out.println(" pass");
	  }//if returns sports: pass
	  else {
		  System.out.println(" fail");
	  }
	
	  
	  String category5 = test.categorize(test5);
	  System.out.print("test5");
	  if(category5 == null) {
		  System.out.println(" fail");
	  }
	  else if(category5.equals("art")){
		  System.out.println(" pass");
	  }//if returns art: pass
	  else {
		  System.out.println(" fail");
	  }
	  
	  
	  String category6 = test.categorize(test6);
	  System.out.print("test6");
	  if(category6 == null) {
		  System.out.println(" pass");
	  }
	  else { 
		  System.out.println(" fail");
	  }//if returns null: pass
	  
	  
	  String category7 = test.categorize(test7);
	  System.out.print("test7");
	  if(category7 == null) {
		  System.out.println(" pass");
	  }
	  else { 
		  System.out.println(" fail");
	  }//if returns null: pass
	   
  	}
}

