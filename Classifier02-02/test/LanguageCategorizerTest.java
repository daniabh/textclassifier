import classifiers.language.LanguageCategorizer;
import classifiers.shared.LabelledText;
import classifiers.language.Language;


import classifier.utilities.ClassifierUtilities;
/**
 * @author Dania Bouhmidi
 * @version 2019-10-23
 * LanguageCategorizerTest serves as test for the LanguageCategorizerTest
 */
public class LanguageCategorizerTest{

	public static void main (String [] args) {
		categorizeTest();
		LanguageCategorizerConstructorTest();
	}
	/**
	 * Tests the LanguageCategorizer Constructor (updated)
	 * @return void
	 */ 


	public static void LanguageCategorizerConstructorTest () {
		int success =0;

		String [] languages = {"French","Arabic","Korean"};
		Language language = new Language(languages);
		LabelledText [] testText = new LabelledText[4];
		testText[0] = new LabelledText("Arabic","السلام عليكم");
		testText[1] = new LabelledText("French","Bonjour");
		testText[2] = new LabelledText("French","Encore");
		testText[3] = new LabelledText("Korean","안녕하세요 여러분 저는 아나이고요 저는 워싱턴 디씨에 살아요");
		LanguageCategorizer C = new LanguageCategorizer (testText,language);
		// LabelledText array should be sorted based on the lexicographical order of the texts
		if (ClassifierUtilities.getArrayasString
				(C.getSentence(0)).equals("bonjour encore") ) {

			success++;
		} 
		if (ClassifierUtilities.getArrayasString
				(C.getSentence(1)).equals("السلام عليكم")) {

			success++;
		} 
		if (ClassifierUtilities.getArrayasString
				(C.getSentence(2)).equals("디씨에 살아요 아나이고요 안녕하세요 여러분 워싱턴 저는") ) {
			success++;
		}

		if(success ==  3) {   
			System.out.println("PASS");
		}
		else {
			System.out.println("FAIL");
		}


	}


	/**
	 * Tests the categorize method
	 * @return void
	 */ 
	public static void categorizeTest() {
		int success =0;
		String [] languages = {"French","English","Arabic","Korean"};
		Language language = new Language(languages);
		LabelledText [] testText = new LabelledText[4];
		testText[0] = new LabelledText("Arabic","السلام عليكم");
		testText[1] = new LabelledText("French","Bonjour, il est actuellement onze heures quarante trois.");
		testText[2] = new LabelledText("English","Hello, I am categorizing texts!");
		testText[3] = new LabelledText("Korean","안녕하세요 여러분 저는 아나이고요 저는 워싱턴 디씨에 살아요");
		LanguageCategorizer C = new LanguageCategorizer (testText,language);

		if ((C.categorize("il il il est onze heures maintenant").equals("French"))) {
			success++;

		}

		if ((C.categorize("Hello everyone").equals("English"))){
			success++;

		}


		if ((C.categorize("nothing")== null)){
			success++;

		}

		if ((C.categorize("السلام عليكم ورحمة الله").equals ("Arabic"))){
			success++;

		}
		if (((C.categorize("안녕하세요")).equals("Korean"))) {
			success++;
		}


		if (success == 5) {
			System.out.println("PASS");
		}
		else {
			System.out.println("FAIL");

		}
	}


}

